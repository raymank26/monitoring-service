FROM java:8

COPY build/libs/monitoring-service-1.0-fat.jar /app/app-1.0-fat.jar

WORKDIR "/app"

CMD ["java", "-jar", "app-1.0-fat.jar"]
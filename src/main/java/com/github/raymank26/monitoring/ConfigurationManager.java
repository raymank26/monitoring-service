package com.github.raymank26.monitoring;

import com.github.raymank26.monitoring.verticles.BrokerVerticle;
import com.github.raymank26.monitoring.verticles.ServerVerticle;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;

/**
 * Configuration provider.
 *
 * @author Anton Ermak
 */
public final class ConfigurationManager {

    private static final Config config = ConfigFactory.load().getConfig("app");

    private ConfigurationManager() {
    }

    public static DeploymentOptions getBrokerConfiguration() {

        return new DeploymentOptions().setConfig(new JsonObject()
                .put(BrokerVerticle.CONFIG_REDIS_HOST, config.getString("redis-host"))
                .put(BrokerVerticle.CONFIG_REDIS_PORT, config.getInt("redis-port"))
                .put(BrokerVerticle.CONFIG_MAX_QUEUE_LENGTH, config.getInt("max-queue-length")));
    }

    public static DeploymentOptions getHttpServerConfiguration() {
        return new DeploymentOptions().setConfig(new JsonObject()
                .put(ServerVerticle.CONFIG_SERVER_HOST, config.getString("http-server-host"))
                .put(ServerVerticle.CONFIG_SERVER_PORT, config.getInt("http-server-port")));
    }
}

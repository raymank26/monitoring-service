package com.github.raymank26.monitoring;

import com.github.raymank26.monitoring.codecs.PairCodec;
import com.github.raymank26.monitoring.codecs.StatusCodec;
import com.github.raymank26.monitoring.verticles.BrokerVerticle;
import com.github.raymank26.monitoring.verticles.ServerVerticle;

import io.vertx.core.Vertx;

/**
 * @author Anton Ermak
 */
public final class Main {
    public static void main(String[] args) {
        System.setProperty("vertx.logger-delegate-factory-class-name",
                "io.vertx.core.logging.SLF4JLogDelegateFactory");

        Vertx vertex = Vertx.vertx();
        vertex.eventBus().registerCodec(new StatusCodec());
        vertex.eventBus().registerCodec(new PairCodec());

        vertex.deployVerticle(new BrokerVerticle(),
                ConfigurationManager.getBrokerConfiguration());
        vertex.deployVerticle(new ServerVerticle(),
                ConfigurationManager.getHttpServerConfiguration());
    }
}

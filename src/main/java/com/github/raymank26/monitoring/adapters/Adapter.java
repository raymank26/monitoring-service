package com.github.raymank26.monitoring.adapters;

import io.vertx.core.json.JsonObject;

/**
 * Adapter for serializing-deserializing models.
 *
 * @author raymank26
 */
public interface Adapter<T> {

    JsonObject serialize(T obj);

    T deserialize(JsonObject src);

    default String serializeToString(T obj) {
        return serialize(obj).toString();
    }

    default T deserializeFromString(String src) {
        return deserialize(new JsonObject(src));
    }

    default JsonObject convertToJsonObject(String src) {
        return serialize(deserializeFromString(src));
    }
}

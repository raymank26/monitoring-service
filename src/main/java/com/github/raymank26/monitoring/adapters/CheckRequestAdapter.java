package com.github.raymank26.monitoring.adapters;

import com.github.raymank26.monitoring.model.CheckRequest;

import io.vertx.core.json.JsonObject;

/**
 * @author Anton Ermak
 */
public final class CheckRequestAdapter implements Adapter<CheckRequest> {

    public static final CheckRequestAdapter INSTANCE = new CheckRequestAdapter();

    private static final String MEMBER_HOST = "host";
    private static final String MEMBER_PATH = "path";
    private static final String MEMBER_PORT = "port";

    private CheckRequestAdapter() {
    }

    public static CheckRequestAdapter getInstance() {
        return INSTANCE;
    }

    @Override
    public JsonObject serialize(CheckRequest obj) {
        return new JsonObject()
                .put(MEMBER_HOST, obj.host)
                .put(MEMBER_PATH, obj.path)
                .put(MEMBER_PORT, obj.port);
    }

    @Override
    public CheckRequest deserialize(JsonObject src) {
        return new CheckRequest(
                src.getString(MEMBER_HOST),
                src.getInteger(MEMBER_PORT),
                src.getString(MEMBER_PATH));
    }
}

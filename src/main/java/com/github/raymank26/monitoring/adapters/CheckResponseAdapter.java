package com.github.raymank26.monitoring.adapters;

import com.github.raymank26.monitoring.model.CheckResponse;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import io.vertx.core.json.JsonObject;

/**
 * @author Anton Ermak
 */
public final class CheckResponseAdapter implements Adapter<CheckResponse> {

    private static final CheckResponseAdapter INSTANCE = new CheckResponseAdapter();

    private static final String MEMBER_STATUS = "status";
    private static final String MEMBER_REASON = "reason";
    private static final String MEMBER_RESPONSE = "response";
    private static final String MEMBER_DATETIME = "datetime";

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

    private CheckResponseAdapter() {
    }

    public static CheckResponseAdapter getInstance() {
        return INSTANCE;
    }

    @Override
    public JsonObject serialize(CheckResponse obj) {
        return new JsonObject()
                .put(MEMBER_STATUS, obj.status)
                .put(MEMBER_REASON, obj.reason)
                .put(MEMBER_RESPONSE, obj.response)
                .put(MEMBER_DATETIME, obj.datetime.format(FORMATTER));
    }

    @Override
    public CheckResponse deserialize(JsonObject src) {
        return new CheckResponse(src.getInteger(MEMBER_STATUS),
                src.getString(MEMBER_REASON),
                src.getString(MEMBER_RESPONSE),
                LocalDateTime.parse(src.getString(MEMBER_DATETIME),
                        FORMATTER));
    }
}

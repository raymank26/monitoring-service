package com.github.raymank26.monitoring.codecs;

import com.github.raymank26.monitoring.model.Pair;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

/**
 * Handy {@link MessageCodec} for sending {@link Pair} instances.
 *
 * @author Anton Ermak
 */
public final class PairCodec implements MessageCodec<Pair, Pair> {

    public static final String CODEC_NAME = "pair-codec";

    @Override
    public void encodeToWire(Buffer buffer, Pair stringStringPair) {
        buffer.appendInt(stringStringPair.key.length());
        buffer.appendInt(stringStringPair.value.length());
        buffer.appendString(stringStringPair.key, stringStringPair.value);
    }

    @Override
    public Pair decodeFromWire(int pos, Buffer buffer) {
        int keyLength = buffer.getInt(pos);
        int valueLength = buffer.getInt(pos);
        String key = buffer.getString(pos + 8, pos + 8 + keyLength);
        String value = buffer.getString(pos + 8 + keyLength, pos + 8 + keyLength + valueLength);
        return new Pair(key, value);
    }

    @Override
    public Pair transform(Pair stringStringPair) {
        return stringStringPair;
    }

    @Override
    public String name() {
        return CODEC_NAME;
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}

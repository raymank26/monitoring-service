package com.github.raymank26.monitoring.codecs;

import com.github.raymank26.monitoring.model.Status;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

/**
 * Handy {@link MessageCodec} for sending {@link Status} instances.
 *
 * @author Anton Ermak
 */
public final class StatusCodec implements MessageCodec<Status, Status> {

    public static final String CODEC_NAME = "status-codec";

    @Override
    public void encodeToWire(Buffer buffer, Status status) {
        if (status == Status.Ok) {
            buffer.appendByte((byte) 0);
        } else {
            buffer.appendByte((byte) 1);
        }
    }

    @Override
    public Status decodeFromWire(int pos, Buffer buffer) {
        if (buffer.getByte(pos) == 0) {
            return Status.Ok;
        } else {
            return Status.Error;
        }
    }

    @Override
    public Status transform(Status status) {
        return status;
    }

    @Override
    public String name() {
        return CODEC_NAME;
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}

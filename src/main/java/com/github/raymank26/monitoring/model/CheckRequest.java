package com.github.raymank26.monitoring.model;

/**
 * The job instance.
 *
 * @author Anton Ermak
 */
public final class CheckRequest {

    public final String host;
    public final int port;
    public final String path;

    public CheckRequest(String host, int port, String path) {
        this.host = host;
        this.port = port;
        this.path = path;
    }

    @Override
    public String toString() {
        return "CheckRequest{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", path='" + path + '\'' +
                '}';
    }
}

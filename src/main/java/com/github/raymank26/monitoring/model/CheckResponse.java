package com.github.raymank26.monitoring.model;

import java.time.LocalDateTime;

/**
 * Result processing of {@link CheckRequest}.
 *
 * @author Anton Ermak
 */
public final class CheckResponse {

    public final int status;
    public final String reason;
    public final String response;
    public final LocalDateTime datetime;

    public CheckResponse(int status, String reason, String response, LocalDateTime datetime) {
        this.status = status;
        this.reason = reason;
        this.response = response;
        this.datetime = datetime;
    }
}

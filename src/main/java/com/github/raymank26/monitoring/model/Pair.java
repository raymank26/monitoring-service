package com.github.raymank26.monitoring.model;

/**
 * @author Anton Ermak
 */
public final class Pair {

    public final String key;
    public final String value;

    public Pair(String key, String value) {
        this.key = key;
        this.value = value;
    }
}

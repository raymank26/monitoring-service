package com.github.raymank26.monitoring.model;

/**
 * Status of database operation.
 *
 * @author Anton Ermak
 */
public enum Status {
    Ok,
    Error
}

package com.github.raymank26.monitoring.verticles;

import com.github.raymank26.monitoring.adapters.Adapter;
import com.github.raymank26.monitoring.adapters.CheckRequestAdapter;
import com.github.raymank26.monitoring.adapters.CheckResponseAdapter;
import com.github.raymank26.monitoring.codecs.StatusCodec;
import com.github.raymank26.monitoring.model.CheckRequest;
import com.github.raymank26.monitoring.model.Pair;
import com.github.raymank26.monitoring.model.Status;

import java.util.function.Consumer;
import java.util.function.Function;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * This verticle represents Redis-backend. The job queue represented as double-linked list. Every
 * time when {@link CheckerVerticle} ask for a job we should pop and push job atomically and send
 * result to checker. The last incoming job will have the minimal priority.
 *
 * Responses are stored in {@link CheckRequest}-{@link com.github.raymank26.monitoring.model
 * .CheckResponse} hash.
 *
 * @author Anton Ermak
 */
public final class BrokerVerticle extends AbstractVerticle {

    public static final String CONFIG_REDIS_HOST = "redis-host";
    public static final String CONFIG_REDIS_PORT = "redis-port";
    public static final String CONFIG_MAX_QUEUE_LENGTH = "max-queue-length";

    private static final int REDIS_TIMEOUT = 2 * 1000;
    private static final int WORKERS_POOL_SIZE = 10;

    private static final String MESSAGE_QUEUE = "queue";
    private static final String RESPONSE_HASH = "hash";

    private static final Logger logger = LoggerFactory.getLogger(BrokerVerticle.class);

    private JedisPool pool;
    private int maxQueueLength;

    @Override
    public void start() throws Exception {
        pool = new JedisPool(new JedisPoolConfig(),
                config().getString(CONFIG_REDIS_HOST),
                config().getInteger(CONFIG_REDIS_PORT),
                REDIS_TIMEOUT);
        maxQueueLength = config().getInteger(CONFIG_MAX_QUEUE_LENGTH);

        vertx.eventBus().consumer(ServerVerticle.EVENT_SAVE_APPLICATION_CHECK, this::saveCheck);
        vertx.eventBus().consumer(CheckerVerticle.EVENT_SAVE_APPLICATION_STATUS, this::saveResult);
        vertx.eventBus().consumer(ServerVerticle.EVENT_GET_APPLICATION_STATUS,
                this::searchForResult);
        vertx.eventBus().consumer(CheckerVerticle.EVENT_GET_WORK, this::getWork);
        initWorkers();
    }

    private void initWorkers() {
        int i = 0;
        while (i < WORKERS_POOL_SIZE) {
            vertx.deployVerticle(new CheckerVerticle(), new DeploymentOptions().setWorker(true));
            i++;
        }
    }

    private void saveCheck(Message<Object> message) {

        CheckRequest checkRequest = CheckRequestAdapter.getInstance().deserialize(
                (JsonObject) message.body());

        putToRedis(jedis -> {
            Long currentLength = jedis.llen(MESSAGE_QUEUE);
            if (currentLength > maxQueueLength) {
                throw new IllegalStateException("queue overflow");
            }
            jedis.lpush(MESSAGE_QUEUE, CheckRequestAdapter.getInstance()
                    .serializeToString(checkRequest));
        }, message);
    }

    private void getWork(Message<Object> msg) {
        fetchFromRedis(jedis -> jedis.rpoplpush(MESSAGE_QUEUE, MESSAGE_QUEUE),
                CheckRequestAdapter.getInstance(), msg);
    }

    private void saveResult(Message<Object> msg) {
        Pair pair = (Pair) msg.body();
        putToRedis(jedis -> jedis.hset(RESPONSE_HASH, pair.key, pair.value), msg);
    }

    private void searchForResult(Message<Object> msg) {
        fetchFromRedis(jedis -> jedis.hget(RESPONSE_HASH, (String) msg.body()),
                CheckResponseAdapter.getInstance(), msg);
    }

    private void putToRedis(Consumer<Jedis> lambda, Message<?> msg) {
        DeliveryOptions deliveryOptions = new DeliveryOptions()
                .setCodecName(StatusCodec.CODEC_NAME);
        vertx.executeBlocking(future -> {
            try (Jedis jedis = pool.getResource()) {
                logger.info("Data saved");
                lambda.accept(jedis);
                future.complete();
            }
        }, true, asyncResult -> {
            if (asyncResult.failed()) {
                handleRedisException(asyncResult.cause());
                msg.reply(Status.Error, deliveryOptions);
            } else {
                msg.reply(Status.Ok, deliveryOptions);
            }
        });
    }

    private <T> void fetchFromRedis(Function<Jedis, String> lambda, Adapter<T> adapter,
                                    Message<?> msg) {
        vertx.executeBlocking(future -> {
            try (Jedis jedis = pool.getResource()) {
                String serializedObject = lambda.apply(jedis);
                future.complete(serializedObject);
            }
        }, true, asyncResult -> {
            if (asyncResult.failed()) {
                handleRedisException(asyncResult.cause());
                msg.fail(0, null);
            } else {
                String serializedObject = (String) asyncResult.result();
                if (serializedObject == null) {
                    msg.reply(null);
                } else {
                    msg.reply(adapter.convertToJsonObject(serializedObject));
                }
            }
        });
    }

    private void handleRedisException(Throwable throwable) {
        logger.warn("redis backend warning", throwable);
    }
}

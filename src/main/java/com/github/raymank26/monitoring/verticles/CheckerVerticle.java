package com.github.raymank26.monitoring.verticles;

import com.github.raymank26.monitoring.adapters.CheckRequestAdapter;
import com.github.raymank26.monitoring.adapters.CheckResponseAdapter;
import com.github.raymank26.monitoring.codecs.PairCodec;
import com.github.raymank26.monitoring.model.CheckRequest;
import com.github.raymank26.monitoring.model.CheckResponse;
import com.github.raymank26.monitoring.model.Pair;

import java.time.LocalDateTime;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * This verticle actually checks destination hosts and send result to {@link BrokerVerticle}.
 *
 * @author Anton Ermak
 */
public final class CheckerVerticle extends AbstractVerticle {

    static final String EVENT_GET_WORK = "work.get";
    static final String EVENT_SAVE_APPLICATION_STATUS = "app-status.save";

    private static final int CONNECTION_TIMEOUT = 2 * 1000;
    private static final int WORKER_PAUSE = 2 * 1000;
    private static final int IDLE_TIMEOUT = 2;

    private static final Logger logger = LoggerFactory.getLogger(CheckerVerticle.class);

    private HttpClient httpClient;

    @Override
    public void start() throws Exception {
        httpClient = vertx.createHttpClient(new HttpClientOptions()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setIdleTimeout(IDLE_TIMEOUT));

        vertx.eventBus().send(EVENT_GET_WORK, new JsonObject(), this::askCheck);
    }

    private void askCheck(AsyncResult<Message<Object>> handler) {
        if (!handler.failed()) {
            JsonObject result = (JsonObject) handler.result().body();
            if (result != null) {
                CheckRequest request = CheckRequestAdapter.getInstance().deserialize(result);
                httpClient.get(request.port, request.host, request.path)
                        .handler(response -> processResponse(request, response))
                        .exceptionHandler(throwable -> processException(request))
                        .end();
            }
        } else {
            waitForWork();
        }

    }

    private void getWork() {
        vertx.eventBus().send(EVENT_GET_WORK, new JsonObject(), this::askCheck);
    }

    private void waitForWork() {
        try {
            Thread.sleep(WORKER_PAUSE);
            getWork();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    // TODO: add response encoding guess.
    private void processResponse(CheckRequest checkRequest, HttpClientResponse response) {
        logger.info("Connection OK");
        response.bodyHandler(buffer -> {
            CheckResponse checkResponse = new CheckResponse(response.statusCode(),
                    response.statusMessage(), buffer.toString("UTF-8"), LocalDateTime.now());
            sendResult(checkRequest, checkResponse);
        });
    }

    private void processException(CheckRequest checkRequest) {
        logger.info("Connection refused - " + checkRequest.toString());
        String message = "Service unavailable";
        sendResult(checkRequest, new CheckResponse(-1, message, message, LocalDateTime.now()));
    }

    private void sendResult(CheckRequest checkRequest, CheckResponse checkResponse) {
        DeliveryOptions deliveryOptions = new DeliveryOptions().setCodecName(PairCodec.CODEC_NAME);
        vertx.eventBus().publish(EVENT_SAVE_APPLICATION_STATUS,
                new Pair(CheckRequestAdapter.getInstance().serializeToString(checkRequest),
                        CheckResponseAdapter.getInstance().serializeToString(checkResponse)),
                deliveryOptions);
        waitForWork();
    }
}

package com.github.raymank26.monitoring.verticles;

import com.github.raymank26.monitoring.adapters.CheckRequestAdapter;
import com.github.raymank26.monitoring.model.CheckRequest;
import com.github.raymank26.monitoring.model.Status;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * This verticle is responsible for async REST-service and provides information to
 * {@link BrokerVerticle}.
 *
 * @author Anton Ermak
 */
public final class ServerVerticle extends AbstractVerticle {

    public static final String CONFIG_SERVER_HOST = "server-host";
    public static final String CONFIG_SERVER_PORT = "server-port";

    static final String EVENT_GET_APPLICATION_STATUS = "app-status.get";
    static final String EVENT_SAVE_APPLICATION_CHECK = "app-check.save";

    private static final String MEMBER_HOST = "host";
    private static final String MEMBER_PATH = "path";
    private static final String MEMBER_PORT = "port";

    private static final Logger logger = LoggerFactory.getLogger(ServerVerticle.class);
    private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    @Override
    public void start() throws Exception {
        String host = config().getString(CONFIG_SERVER_HOST);
        Integer port = config().getInteger(CONFIG_SERVER_PORT);

        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());
        router.exceptionHandler(this::handleThrowable);

        router.post("/api/v1/application-check")
                .handler(this::handleApplicationCheck);

        router.get("/api/v1/application-status/:host/:port/:path")
                .handler(this::handleApplicationStatus)
                .produces(CONTENT_TYPE_APPLICATION_JSON);

        vertx.createHttpServer()
                .requestHandler(router::accept).listen(port, host, this::listenHandler);
        logger.info("Server has started at http://" + host + ":" + port);
    }

    private static boolean validRequestJson(JsonObject json) {
        return json.containsKey(MEMBER_HOST) && json.containsKey(MEMBER_PORT) &&
                json.containsKey(MEMBER_PATH);
    }

    private void listenHandler(AsyncResult<HttpServer> httpServer) {
        if (httpServer.failed()) {
            handleThrowable(httpServer.cause());
            // TODO: shutdown gracefully
            Runtime.getRuntime().exit(1);
        }
    }

    private void handleApplicationCheck(RoutingContext context) {
        JsonObject json = context.getBodyAsJson();
        if (validRequestJson(json)) {

            JsonObject checkRequestJsonObject = CheckRequestAdapter.getInstance().serialize(
                    new CheckRequest(json.getString(MEMBER_HOST), json.getInteger(MEMBER_PORT),
                            json.getString("path")));

            vertx.eventBus().send(EVENT_SAVE_APPLICATION_CHECK, checkRequestJsonObject, async -> {
                Status status = (Status) async.result().body();
                int statusCode = status == Status.Ok ? 201 : 503;
                context.response().setStatusCode(statusCode).end();
            });
        } else {
            context.response().setStatusCode(400);
        }
    }

    private void handleApplicationStatus(RoutingContext context) {
        HttpServerRequest request = context.request();

        String host = request.getParam(MEMBER_HOST);
        int port = Integer.parseInt(request.getParam(MEMBER_PORT));
        String path = null;
        // TODO: ugly hack. See my bug report https://github.com/vert-x3/vertx-web/issues/236
        try {
            path = URLDecoder.decode(request.getParam(MEMBER_PATH), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            handleThrowable(e);
            context.response().setStatusCode(400).end();
        }

        String checkRequest = CheckRequestAdapter.getInstance().serializeToString(
                new CheckRequest(host, port, path));
        vertx.eventBus().send(EVENT_GET_APPLICATION_STATUS, checkRequest, async -> {
            Message<Object> msg = async.result();
            if (msg == null) { // error
                context.response().setStatusCode(503).end();
            } else {
                JsonObject body = (JsonObject) msg.body();
                if (body == null) {
                    context.response().setStatusCode(404).end();
                } else {
                    context.response()
                            .setStatusCode(200)
                            .putHeader("content-type", CONTENT_TYPE_APPLICATION_JSON)
                            .end(body.toString());
                }
            }
        });
    }

    private void handleThrowable(Throwable handler) {
        logger.error(handler);
    }
}
